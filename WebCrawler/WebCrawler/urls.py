from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic import RedirectView
import views as v


urlpatterns = [
    url('^login/', v.login,  name='login'),
    url('^logout/', v.logout, name='logout'),
    url('^editar_dados_usuario/', v.editar_dados_usuario, name='editar_dados_usuario'),
    url('^novo_usuario/$', v.novo_usuario, name="novo_usuario"),
    url('^novo_usuario/cadastrar/', v.cadastrar_novo_usuario, name="cadastrar_novo_usuario"),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^WebCrawler/', include('WebCrawlerEngine.urls', namespace='WebCrawler')),
    url(r'^$', RedirectView.as_view(url="/WebCrawler", permanent=True)),
]
