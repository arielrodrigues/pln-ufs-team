#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function

import argparse
import codecs
import json
import sys

import util
from recursos import Recursos

from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream


class FileListener(StreamListener):
    def __init__(self, args):
        StreamListener.__init__(self)

        if (args.pontuacao):
            util.carregar_pontuacao()
        if (args.stopwords):
            util.carregar_stop_words()

        self.args = args
        self.rec = Recursos(args.usuario)
        self.contagem_palavras = 0
        self.contagem_twittes = 0
        self.qtd_palavras = 0

    def salve_contagem(self):
        with open(self.rec.nome_padrao_arquivo_contagem_palavra,"w") as f:
            f.write(str(self.contagem_palavras))
        with open(self.rec.nome_padrao_arquivo_contagem_twittes,"w") as f:
            f.write(str(self.contagem_twittes))

    def on_data(self, data):
        text = ""
        try:
            text = json.loads(data)["text"]
            palavras, text = util.processe_string(self.args, text, "utf-8")
            with codecs.open(self.rec.nome_padrao_corpora, "a", "utf-8") as f:
                f.write("%s\n" % text)
            self.contagem_palavras += palavras
            self.contagem_twittes += 1
            self.salve_contagem()
        except KeyError as e:
            print ("Erro ao procurar por texto no twitte: " + str(text))
        except UnicodeDecodeError as e:
            print (text)
            print ("Erro unicode ao processar twitte :" + str(e))
        except Exception as e:
            print ("Erro desconhecido: " + str(e))


        if self.args.qtd_palavras and self.contagem_palavras > self.args.qtd_palavras:
                return False

        return True

    def on_error(self, status):
        print(status)

def carregar_dados_acesso_twitter(nome_usuario):
    rec = Recursos(nome_usuario)
    return util.ler_arquivo_json(rec.nome_arquivo_dados_acesso_twitter)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Gera arquivo unico de corpora, enquanto o twitter crawler está em exucação, e conta suas palavras")
    parser.add_argument('-usuario', "--usuario", help='Identificação do usuário.')
    parser.add_argument('-qtd_palavras', help='Quantidade aproximada de palavras a se buscar.', type=int)
    parser.add_argument('-palavras_chave', help='Palavras-chave para filtro, se houver.')
    parser.add_argument('--pontuacao', help='Eliminar pontuação', action='store_true')
    parser.add_argument('--stopwords', help='Eliminar stopwords', action='store_true')
    parser.add_argument('--limpar_caracteres', help='Limpar caracteres especiais.', action='store_true')

    args = parser.parse_args(sys.argv[1:])
    args.palavras_chave = [palavra.decode("utf-8") for palavra in args.palavras_chave.split(",") if palavra]

    dados = carregar_dados_acesso_twitter(args.usuario)

    auth = OAuthHandler(dados["consumer_key"], dados["consumer_secret"])
    auth.set_access_token(dados["access_token"], dados["access_token_secret"])

    l = FileListener(args)

    stream = Stream(auth, l)
    if (args.palavras_chave):
        stream.filter(track=args.palavras_chave, languages=("pt","br"))
    else:
        stream.sample(languages=("pt","br"))
