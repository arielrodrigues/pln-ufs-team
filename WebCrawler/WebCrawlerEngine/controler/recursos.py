from inspect import currentframe
import util
from os.path import join


class Recursos():
    nome_diretorio_global_recursos = util.obter_diretorio_recurso(currentframe(), ["usr"])
    nome_diretorio_global_media = util.obter_diretorio_recurso(currentframe(), ["media"])

    def __init__(self, nome_usuario):
        self.nome_usuario = nome_usuario

    @staticmethod
    def nome_diretorio_ner():
        return join(Recursos.nome_diretorio_global_recursos, "ner")

    @property
    def nome_diretorio_usuario(self):
        return join(self.nome_diretorio_global_media, self.nome_usuario)

    @property
    def nome_diretorio_crawler(self):
        return join(self.nome_diretorio_global_recursos, "crawler")

    @property
    def nome_diretorio_crawler_usuario(self):
        return self.nome_diretorio_usuario

    @property
    def nome_diretorio_data_target_usuario(self):
        return join(self.nome_diretorio_usuario,"data","data_target")

    @property
    def nome_diretorio_padrao_destino_corpora(self):
        return join(self.nome_diretorio_crawler_usuario, "corpus")

    @property
    def nome_padrao_corpora(self):
        return join(self.nome_diretorio_padrao_destino_corpora, "corpus.txt")

    @property
    def nome_padrao_arquivo_dados_processamento(self):
        return join(self.nome_diretorio_usuario, "monitor.json")

    @property
    def nome_padrao_arquivo_contagem_palavra(self):
        return join(self.nome_diretorio_padrao_destino_corpora, "words_count.txt")

    @property
    def nome_padrao_arquivo_contagem_paginas(self):
        return join(self.nome_diretorio_padrao_destino_corpora, "pages_count.txt")

    @property
    def nome_padrao_arquivo_contagem_twittes(self):
        return join(self.nome_diretorio_padrao_destino_corpora, "twittes_count.txt")

    @property
    def nome_completo_arquivo_perplexidade(self):
        return join(self.nome_diretorio_padrao_destino_corpora, "corpus_teste.txt")

    @property
    def nome_padrao_corpora_compactado(self):
        return "corpus_data.tar.gz"

    @property
    def destino_arquivo_compactado(self):
        return self.nome_diretorio_usuario

    @property
    def nome_completo_arquivo_compactado(self):
        return join(self.destino_arquivo_compactado, self.nome_padrao_corpora_compactado)

    @property
    def nome_arquivo_dados_acesso_twitter(self):
        return join(self.nome_diretorio_usuario, "twitter.conf")




