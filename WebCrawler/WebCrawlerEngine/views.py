# -*- coding: utf-8 -*-
import os
from rexec import FileWrapper
from django.contrib.auth.decorators import login_required
from tweepy.error import TweepError
from controler.recursos import Recursos
from django.http.response import HttpResponse
from django.shortcuts import render, redirect
from controler.util import ler_urls_arquivo, get_tempo_execucao_formatado, get_numero_formatado, ler_dados_acesso_twitter_arquivo
from controler.crawler_factory import CrawlerFactory


def get_crawler_factory(user):
    recursos = Recursos(user.email)
    return CrawlerFactory(user.email, recursos)


def get_crawler_em_execucao(user):
    cf = get_crawler_factory(user)
    return cf.get_crawler_em_execucao()


@login_required(login_url='/login/')
def index(request):
    crawler_facade = get_crawler_em_execucao(request.user)
    if crawler_facade.processamento_em_execucao:
        return redirect('WebCrawler:processamento')

    context = {"corpus_gerado": crawler_facade.corpus_compactado_gerado,
               "user": request.user}

    return render(request, "WebCrawlerEngine/index.html", context)


def get_dominios_selecionados(dominios):
    if "Todos" in dominios:
        dominios.remove("Todos")
        return dominios
    return dominios


@login_required(login_url='/login/')
def processamento(request):
    crawler_facade = get_crawler_em_execucao(request.user)

    context = {"status_processamento": crawler_facade.status_processamento,
               "processamento_em_execucao": crawler_facade.processamento_em_execucao,
               "contagem_palavras": get_numero_formatado(str(crawler_facade.contagem_palavras)),
               "tempo_execucao": get_tempo_execucao_formatado(crawler_facade.tempo_processamento),
               "abrangencia": crawler_facade.abrangencia,
               "user": request.user}
    if crawler_facade.abrangencia == "twitter":
        context["contagem_twittes"] = get_numero_formatado(str(crawler_facade.contagem_twittes))
    else:
        context["contagem_paginas"] = get_numero_formatado(str(crawler_facade.contagem_paginas))
    return render(request, "WebCrawlerEngine/processamento.html", context)


@login_required(login_url='/login/')
def parar_processamento(request):
    crawler_facade = get_crawler_em_execucao(request.user)
    crawler_facade.parar_crawler()
    return redirect("WebCrawler:processamento", permanent=True)


@login_required(login_url='/login/')
def download_corpora(request):
    nome_arquivo = Recursos(request.user.email).nome_completo_arquivo_compactado
    wrapper = FileWrapper(file(nome_arquivo))
    response = HttpResponse(wrapper, content_type='application/x-gzip')
    response['Content-Disposition'] = 'attachment; filename= %s' % os.path.basename(nome_arquivo)
    response['Content-Length'] = os.path.getsize(nome_arquivo)
    return response


@login_required(login_url='/login/')
def iniciar(request):
    if request.method == "GET":
        return render(request, "WebCrawlerEngine/index.html", {"user": request.user})

    crawler_facade = get_crawler_em_execucao(request.user)
    if crawler_facade.processamento_em_execucao:
        return redirect("WebCrawler:processamento", permanent=True)

    cf = get_crawler_factory(request.user)
    palavras_chave = request.POST.get(u'text_palavras_chave', "").encode("utf-8").split(",")
    abrangencia = request.POST.get(u'optionsRadio_abrangencia')
    arquivo_conf_twitter = request.FILES.get(u'arquivo_dados_acesso_twitter')
    opcoes = {"ativar_ngram": bool(int(request.POST.get(u'checkbox_ativarngram', False))),
              "ativar_entidades": bool(int(request.POST.get(u'checkbox_ativarentidades', False))),
              "gerar_vocabulario":  bool(int(request.POST.get(u'checkbox_vocabulario', False))),
              "formato_saida_entidades":request.POST.get(u'formato_saida_entidades'),
              "qtd_palavras": "" if not request.POST.get(u'input_qtd_palavras') else int(request.POST.get(u'input_qtd_palavras')),
              "numero_gram" : int(request.POST.get(u'numero_gram', 0)),
              "abrangencia": abrangencia,
              "palavras_chave": palavras_chave,
              "limpar_caracteres": bool(int(request.POST.get("checkbox_limparcaracteres", False))),
              "eliminar_pontuacao":bool(int(request.POST.get("checkbox_eliminarpontuacao", False))),
              "eliminar_stopwords": bool(int(request.POST.get("checkbox_eliminarstopwords", False)))}

    if abrangencia == "web":
        crawler_facade = cf.get_news_crawler()
    else:
        crawler_facade = cf.get_twitter_crawler()
        configuracoes = {}
        erros = {}
        try:
            configuracoes = ler_dados_acesso_twitter_arquivo(arquivo_conf_twitter, request.user.email)
            crawler_facade.validar_credenciais_twitter(configuracoes)
        except TweepError as e:
            erros = {"erros": ["Não foi possível se contectar ao twitter usando as credenciais informadas"]}
        except Exception as e:
            erros = {"erros":(str(e),)}

        if "erros" in erros:
            return render(request, "WebCrawlerEngine/index.html", erros)

        opcoes.update(configuracoes)
        crawler_facade.iniciar_twitter_crawler(opcoes)

        return redirect('WebCrawler:processamento', permanent=True)

    opcao_coleta_selecionada = request.POST.get(u'fonte_coleta')

    dominios = []
    urls_usuario = []

    if opcao_coleta_selecionada == "palavras_chave":

        # É preciso que o usuário tenha informado pelo menos uma palavra chave
        if not palavras_chave:
            # Exibir mensagem de erro e mandar renderizar
            return render(request, 'WebCrawlerEngine/index.html', {"user": request.user})
        crawler_facade.iniciar_web_crawler_com_palavras_chave(**opcoes)
        return redirect('WebCrawler:processamento', permanent=True)

    if opcao_coleta_selecionada == "urls_sementes":
        # É preciso haver, nesse caso, pelo menos um domínio selecionado
        dominios = get_dominios_selecionados(request.POST.getlist(u"slc_dominios[]", []))

    elif opcao_coleta_selecionada == "palavras_chave":
        palavras_chave = request.POST.get(u'text_palavras_chave', "").split(",")
        # É preciso que o usuário tenha informado pelo menos uma palavra chave
        if not palavras_chave:
            # Exibir mensagem de erro e mandar renderizar
            return render(request, 'WebCrawlerEngine/index.html', {"user": request.user})

    elif opcao_coleta_selecionada == "arquivo_urls":
        arquivo_urls = request.FILES.get(u'coleta_arquivo_file')
        try:
            urls_usuario = ler_urls_arquivo(arquivo_urls, request.user.email)
        except Exception as e:
            return render(request, 'WebCrawlerEngine/index.html', {"user": request.user, "erros": (str(e),)})

    crawler_facade.iniciar_web_crawler_com_urls_sementes(urls_usuario= urls_usuario, dominios= dominios, **opcoes)

    return redirect('WebCrawler:processamento', permanent=True)
